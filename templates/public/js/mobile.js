$(document).ready(function () {

    $('.hamburger a').on('click', function () {
        event.preventDefault();
        $('.hamburger').css({
            "height": 0,
            "opacity": 0
        });
        $('.mobile-menu').fadeIn("slow", function () {

        })
        $('body').css({
            "overflow": "hidden"
        })
    });
    $('.close-menu').on('click', function () {
        event.preventDefault();
        $('.hamburger').css({
            "height": 40,
            "opacity": 1
        });
        $('.mobile-menu').fadeOut("slow", function () {

        })
        $('body').css({
            "overflow": "auto"
        })
    });
});