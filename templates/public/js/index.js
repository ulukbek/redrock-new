/**
 * Created by Nurs on 15.04.2017.
 */
$(document).ready(function () {
    $(document).ready(function () {
        $('.left-side ul li a').on('click', function (e) {
            if ($(this).hasClass('false_link')) {
                e.preventDefault();
                var getblock = $(this).attr('href').split('#');
                var id = "#"
                getblock[1] = id + getblock[1]
                $('body,html').animate({scrollTop: $(getblock[1]).offset().top}, 300);
            }
        });
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function () {
                    return $(".photo-title").html();
                }
            }
        });
    });
    $('.callback').on('click', function () {
        event.preventDefault();
        var get_block = $(this).attr('href');
        if ($(get_block).length !== 0) {
            $('html, body').animate({scrollTop: $(get_block).offset().top}, 1000);
        }
    });

    $('.menu_list li a').on('click', function () {
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
        if ($(this).hasClass('data-link')) {
            event.preventDefault();
            var get_block = $(this).attr('href');
            if ($(get_block).length !== 0) {
                $('html, body').animate({scrollTop: $(get_block).offset().top}, 1000);
            }
        }
    })
    var dots_width = $('.main_slider_dots').width()
    $('.main_slider_dots').css({
        "margin-left": dots_width / 2
    })
    $('.main_slider').slick({
        arrows: false,
        dots: true,
        speed: 1000,
        draggable: false,
        autoplay: true,
    });
    $('.travelers-photo-slider').slick({
        dots: false,
        centerMode: true,
        slidesToShow: 3,
        centerPadding: '100px',
        nextArrow: '<button type="button" class="next-photo">',
        prevArrow: '<button type="button" class="prev-photo">',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    centerPadding: '20px',
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    dots: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    centerMode: false,
                    arrows: false,
                    autoplay: true,
                    autoplaySpeed: 2000,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    dots: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                    arrows: false,
                    autoplay: true,
                    autoplaySpeed: 2000,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });
    $('.news_blog_slider').slick({
        dots: false,
        slidesToShow: 3,
        nextArrow: '<button type="button" class="next-news">',
        prevArrow: '<button type="button" class="prev-news">', responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    dots: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false,
                    autoplay: true,
                    autoplaySpeed: 2000,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    dots: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    autoplay: true,
                    autoplaySpeed: 2000,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    })
});