from django.shortcuts import render
from about_us.models import *

# Create your views here.
from main.views import generate_view_params


def index(request):
    context = {

    }
    context.update(generate_view_params(request))
    return render(request, 'article.html', context)
