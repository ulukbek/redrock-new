from django.core.mail import EmailMessage
from django.shortcuts import render

from main.forms import SubmitForm

# Create your views here.
from main.views import generate_view_params
from tours.models import Tour, Days, Category


def index(request):
    context = dict()
    filters = dict()
    direction = request.GET.get('direction')
    if direction is not None:
        direction_object = Category.objects.get(title=direction)
        filters['category'] = direction_object
        context.update({'direction': direction})
    tours = Tour.objects.filter(**filters).all()
    context.update({
        'tour': tours,
        'category': Category.objects.all(),
    })
    context.update(generate_view_params(request))
    return render(request, 'tours.html', context=context)


def tour_page(request, tour_id):
    count = 0
    tour = Tour.objects.get(id=tour_id)
    days = Days.objects.filter(tour=tour)

    context = {
        'days': days,
        'id': tour,
        'tour': Tour.objects.all(),
        'count': count,
        'form': SubmitForm(request.POST),
    }
    context.update(generate_view_params(request))
    return render(request, 'tour-page.html', context=context)


def send_notification_email(title, body, to):
    email = EmailMessage(title, body=body, to=[to])
    email.content_subtype = 'html'
    email.send()
