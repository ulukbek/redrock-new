from django.shortcuts import render
from about_us.models import *


# Create your views here.
from main.views import generate_view_params


def index(request):
    context = {
        'about_us': AboutUs.objects.first(),
        'our_team': OurTeam.objects.all(),
        'testimonials': Testimonials.objects.all()
    }
    context.update(generate_view_params(request))
    return render(request, 'about-us.html', context)
