"""Redrock_Backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from main.views import *
from tours.views import index as tours
from tours.views import tour_page as tour_page
from blog.views import index as blog
from things_to_do.views import things_page as things_page, all_things_to_do
from main import views as main_views
from tripplanner import views as trip_planner
from about_us.views import index as about_us
from contacts.views import index as contacts
from main.views import subscriber_create, subscriber_delete
from Redrock_Backend import settings
from article.views import index as article

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^$', index, name='index'),
    url(r'^about_our_country/$', article, name='article'),
    url(r'^subscribe/$', subscriber_create, name='subscriber_create'),
    url(r'^subscribe_delete_page/$', subscriber_delete_page, name='subscriber_delete_page'),
    url(r'^subscribe_delete/$', subscriber_delete, name='subscriber_delete'),
    url(r'^contacts/$', contacts, name='contacts'),
    url(r'^our_team/$', about_us, name='about_us'),
    url(r'^things_to_do/(?P<things_id>\d+)/$', things_page, name='things_page'),
    url(r'^all_things_to_do/$', all_things_to_do, name='all_things_to_do'),
    # url(r'^tripplanner/$', tripplanner, name='tripplanner'),
    url(r'^tours/$', tours, name='tours'),
    url(r'^tours/(?P<tour_id>\d+)/$', tour_page, name='tour_page'),
    url(r'^blog/(?P<blog_id>\d+)/$', blog, name='blog_page'),
    url(r'^admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^feedback$', main_views.feedback, name='feedback'),
    # url(r'^trip/fixtures$', trip_planner.fixtures, name='trip_fixtures'),
    # url(r'^trip/hotel/get/by/direction$', trip_planner.get_hotels_by_direction, name='get_hotels_by_direction')
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
